package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetX() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals(10, rover.getPlanetX() );
		
	}
	
	@Test
	public void testPlanetY() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals(12, rover.getPlanetY() );
		
	}
	@Test
	public void testPlanetObstacles() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(5,5)", rover.getPlanetObstacles().get(0) );
		
	}
	
	@Test
	public void testPlanetObstaclesAtXYCoordinates() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertTrue( rover.planetContainsObstacleAt(5, 5) );
		
	}
	
	@Test
	public void testLandingStatus() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,N)", rover.getLandingStatus() );
		
	}
	
	@Test
	public void testCommandExecutionWithEmptyCommandString() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,N)", rover.executeCommand("") );
		
	}
	
	@Test
	public void testCommandExecutionTurnRight() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r") );
		
	}
	
	@Test
	public void testCommandExecutionTurnLeft() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,W)", rover.executeCommand("l") );
		
	}
	
	@Test
	public void testCommandExecutionGoForward() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,1,N)", rover.executeCommand("f") );
		
	}
	
	@Test
	public void testCommandExecutionGoBackward() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.setLandingStatus("(5,8,E)");
		
		assertEquals("(4,8,E)", rover.executeCommand("b") );
		
	}
	
	@Test
	public void testCommandExecutionWithMultipleCommands() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<String>();
		
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff") );
		
	}
	
	

}
