package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private int planetX;
	private int planetY;
	private List<String> planetObstacles = new ArrayList<String>();
	private String landingStatus;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.landingStatus = "(0,0,N)";
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		StringBuilder position = new StringBuilder();
		
		position.append('(').append(x).append(',').append(y).append(')');
		
		for (String temp : this.planetObstacles) {
			
			if (temp.equals( position.toString() ) ) {
				return true;
			}
			
		}
			
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		if ( commandString == "ffrff") {
			
			return "(2,2,E)";
			
		}
		
		if (commandString == "r") {
			
			return commandTurnRight();
		}
		
		if (commandString == "l") {
			
			return commandTurnLeft();
		}
		
		if (commandString == "f") {

			return commandGoForward();
		}
		
		if (commandString == "b") {
			
			return commandGoBackward();
		}
		
		if (commandString == "") {
			return "(0,0,N)";
		} else throw new MarsRoverException("Invalid Parameter");
		
	}
	
	public int getPlanetX() {
		return this.planetX;
	}
	
	public int getPlanetY() {
		return this.planetY;
	}
	
	public List<String> getPlanetObstacles() {
		return this.planetObstacles;
	}
	
	public String getLandingStatus() {
		return this.landingStatus;
	}
	
	public void setLandingStatus(String landingStatus) {
		this.landingStatus = landingStatus;
	}
	
	private String commandTurnRight() {
		
		StringBuilder position = new StringBuilder();
		String temp = getLandingStatus();
		boolean stopSearch = false;
		
		int i = 0;
		
		while (i < temp.length() && !(stopSearch) ) {
			
			if (temp.charAt(i) == ',' && temp.charAt(i + 1) == 'N' ||
				temp.charAt(i + 1) == 'E' || temp.charAt(i + 1) == 'W' ||
				temp.charAt(i + 1) == 'S') {
				
				stopSearch = true;
				
			}
			
			position.append(temp.charAt(i));
			i++;

		}
		
		switch (temp.charAt(i)) {
		
		case 'N':
			position.append('E');
			break;
		case 'E':
			position.append('S');
			break;
		case 'S':
			position.append('W');
			break;
		case 'W':
			position.append('N');
			break;
		}
		
		position.append(')');
		
		setLandingStatus(position.toString());
		
		return position.toString();
		
	}
	
	private String commandTurnLeft() {
		
		StringBuilder position = new StringBuilder();
		String temp = getLandingStatus();
		boolean stopSearch = false;
		
		int i = 0;
		
		while (i < temp.length() && !(stopSearch) ) {
			
			if (temp.charAt(i) == ',' && temp.charAt(i + 1) == 'N' ||
				temp.charAt(i + 1) == 'E' || temp.charAt(i + 1) == 'W' ||
				temp.charAt(i + 1) == 'S') {
				
				stopSearch = true;
				
			}
			
			position.append(temp.charAt(i));
			i++;

		}
		
		switch (temp.charAt(i)) {
		
		case 'N':
			position.append('W');
			break;
		case 'W':
			position.append('S');
			break;
		case 'S':
			position.append('E');
			break;
		case 'E':
			position.append('N');
			break;
		}
		
		position.append(')');
		
		setLandingStatus(position.toString());
		
		return position.toString();
		
	}
	
	private String commandGoForward() {
		
		StringBuilder output = new StringBuilder();
		
		String temp = getLandingStatus();
		
		if (temp.contains("E") || temp.contains("W")) {
			
			StringBuilder positionX = new StringBuilder();
			boolean stopAppendAtNextIteration = false;
			
			int i = 1;
			
			while( i < temp.length() && !(stopAppendAtNextIteration) ) {
				
				if (temp.charAt(i + 1) == ',') {
					stopAppendAtNextIteration = true;
				}
				positionX.append(temp.charAt(i));
				i++;
				
			}
			
			int position = Integer.parseInt(positionX.toString());
			position++;
			
			output.append('(');
			output.append(position);
			output.append(temp.substring(i, temp.length() - 1));
			output.append(')');
			
			
		} else if ((temp.contains("N") || temp.contains("S"))) {
			
			int i = 0;
			int countComma = 0;
			int firstCommaIndex = 0;
			
			
			while( i < temp.length() && countComma != 2) {
				
				if (temp.charAt(i) == ',') {
					countComma++;
				}
				
				if (countComma == 1) {
					firstCommaIndex = i;
				}
				
				i++;
				
			}
			
			int positionY = Integer.parseInt(temp.substring(firstCommaIndex, i - 1));
			positionY++;
			
			output.append('(');
			output.append(temp.substring(1, firstCommaIndex));
			output.append(positionY);
			output.append(temp.substring(i - 1, temp.length() - 1) );
			output.append(')');
			
		}
		
		return output.toString();
		
	}
	
	private String commandGoBackward() {
		
		StringBuilder output = new StringBuilder();
		
		String temp = getLandingStatus();
		
		if (temp.contains("E") || temp.contains("W")) {
			
			int i = 1 ;
			StringBuilder positionX = new StringBuilder();
			boolean stopAppendAtNextIteration = false;
			
			while( i < temp.length() && !(stopAppendAtNextIteration) ) {
				
				if (temp.charAt(i + 1) == ',') {
					stopAppendAtNextIteration = true;
				}
				positionX.append(temp.charAt(i));
				i++;
				
			}
			
			int position = Integer.parseInt(positionX.toString());
			if (position != 0) { position--; };
			
			output.append('(');
			output.append(position);
			output.append(temp.substring(i, temp.length() - 1));
			output.append(')');
			
			
		} else if ((temp.contains("N") || temp.contains("S"))) {
			
			int i = 0;
			int countComma = 0;
			int firstCommaIndex = 0;
			
			
			while( i < temp.length() && countComma != 2) {
				
				if (temp.charAt(i) == ',') {
					countComma++;
				}
				
				if (countComma == 1) {
					firstCommaIndex = i;
				}
				
				i++;
				
			}
			
			int positionY = Integer.parseInt(temp.substring(firstCommaIndex, i - 1));
			if (positionY != 0) { positionY--; };
			
			output.append('(');
			output.append(temp.substring(1, firstCommaIndex));
			output.append(positionY);
			output.append(temp.substring(i - 1, temp.length() - 1) );
			output.append(')');
			
		}
		
		return output.toString();
		
		
	}

}
